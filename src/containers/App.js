import React, {Component} from 'react'
import {connect} from 'react-redux'
import './App.css'
import {User} from "../conponents/User";
import {Page} from "../conponents/Page";
import {setYear} from "../actions/PageActions";

class App extends Component {
    render() {
        const {user, page, setYearAction} = this.props
        return (
            <div className="App">
                <header className="App-header">
                    <h1 className="App-title">Мой топ фото</h1>
                </header>
                <User name={user.name}/>
                <Page photos={page.photos} year={page.year} setYear={setYearAction}/>
            </div>
        )
    }
}

const mapStateToProps = store => {
    console.log(store);
    return {
        user: store.user,
        page: store.page,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setYearAction: year => dispatch(setYear(year)),
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App)
